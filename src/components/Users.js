import '../assets/scss/Body.scss'
import {
  Chart as ChartJS,
  RadialLinearScale,
  ArcElement,
  Tooltip,
  Legend,
} from 'chart.js';
import { PolarArea } from 'react-chartjs-2';
ChartJS.register(RadialLinearScale, ArcElement, Tooltip, Legend);
export const data = {
    labels: ['cat#1', 'cat#2', 'cat#3', 'cat#4'],
    datasets: [
      {
        label: '# of Votes',
        data: [2000, 2500, 1700, 1000],
        backgroundColor: [
          '#725E9C',
          '#5C8F94',
          '#EBA45E',
          '#E4EAEB'
        ],
        borderWidth: 0,
        borderJoinStyle: 'round'
      },
    ],
  };

export const option = {
    plugins: {
        title: {
          display: false,          
        },
       legend: { 
          display: true, 
          position: "bottom",
        }
    },
    scales: {
      r: {
        ticks: {
          display: false 
        },
        grid: {
          display: false 
        }
      }
    },
    elements:{
      arc: {
        angle: 90,        
      }
    }, 
}
function Users(props){
    return(
        <PolarArea data={data} options = {option} />
    )
}

export default Users
import '../assets/scss/Body.scss'
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler,
  ScriptableContext
} from "chart.js";
import { Line } from 'react-chartjs-2';
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
  Filler
);
export const options = {
  maintainAspectRatio: true,
  responsive: true,
  scales: {
    y: {
      grid: {
        display: false
      }
    }
  },
  elements: {
    line: {
      tension: 0.35
    },
    point:{
        radius: 0
    }
  },
  plugins: {
    filler: {
      propagate: false
    },
    legend: { 
      display: false
    }
  },
  interaction: {
    intersect: true
  },
};

const labels = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];


export const data = {
  labels,
  datasets: [
    {
      label: "First dataset",
      data: [230, 530, 350, 410, 440, 650, 550],     
      fill: "start",
      backgroundColor: (context: ScriptableContext<"line">) => {
        const ctx = context.chart.ctx;
        const gradient = ctx.createLinearGradient(0, 0, 0, 200);
        gradient.addColorStop(0, "rgba(120, 151, 100, 0.65)");
        gradient.addColorStop(1, "rgba(255, 255, 255)");
        return gradient;
      },
      borderColor: "#789764",
      borderWidth: 2,
      pointBorderWidth: 0,
      pointBackgroundColor: "#789764"
    }
  ],
}
function Revenue(props){
    return(
        <Line data={data} options = {options} height={100} />
    )
}

export default Revenue